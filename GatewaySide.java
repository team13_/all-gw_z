package javaapplication4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author ZeeYo
 */



public class JavaApplication4 {



static class MyTask implements Runnable
{
   boolean running = false;
   public void start() {
        running = true;
        new Thread(this).start();
    }

public void run() {

    while(running) {

        System.out.println("\ntesting\n");

         try {
            Thread.sleep(1000);
              // you were doing thread.sleep()! sleep is a static function
            //System.out.println("This will be printed");
        Socket clientSocket = null;
        try {
            clientSocket = new Socket("127.0.0.1", 6191);
        } catch (IOException ex) {
            Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex);
        }
    InputStream is = null;
       try {
           is = clientSocket.getInputStream();
       } catch (IOException ex) {
           Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex);
       }
    PrintWriter pw = null;
       try {
           pw = new PrintWriter(clientSocket.getOutputStream());
       } catch (IOException ex) {
           Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex);
       }
    pw.println("GET / HTTP/1.0");
    pw.println();
    pw.flush();
    byte[] buffer = new byte[1024];
    int read;
       try {
           read = is.read(buffer);
          // while((read = is.read(buffer)) != -1) {
               String output = new String(buffer, 0, read);
               System.out.print(output);
               //int a=output.indexOf(":");
               //System.out.print(a);
               String only_id= output.substring(output.indexOf(":")+1,output.indexOf(","));
               String only_time=output.substring(output.lastIndexOf(":") -5);
               System.out.print("\n"+only_id+" "+only_time+"\n");
               System.out.flush();
               JavaApplication4 obj = new JavaApplication4();

        String url = "https://dreamteam13.softwareengineeringii.com/heartbeatsapi/diag?ID="+only_id+"&timestamp="+only_time;

        HttpURLConnection httpClient =
                (HttpURLConnection) new URL(url).openConnection();

        // optional default is GET
        httpClient.setRequestMethod("GET");

        //add request header
        httpClient.setRequestProperty("User-Agent", "Mozilla/5.0");

        int responseCode = httpClient.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(httpClient.getInputStream()))) {

            StringBuilder response = new StringBuilder();
            String line;

            while ((line = in.readLine()) != null) {
                response.append(line);
            }

            //print result
            System.out.println(response.toString());

        }



           //}
         } catch (IOException ex) {
           Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex);
       }
;
       try {
           clientSocket.close();
       } catch (IOException ex) {
           Logger.getLogger(JavaApplication4.class.getName()).log(Level.SEVERE, null, ex);
       }


    }
      catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
}


    public static void main(String[] args) {

        // TODO code application logic here

        new MyTask().start();

}

 }
